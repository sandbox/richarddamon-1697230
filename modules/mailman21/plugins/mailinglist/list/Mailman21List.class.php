<?php
/**
 * @file
 * Definition of MailinglistGenericList class.
 */

/**
 * Mailman 2.1.x List Personality Plugin.
 */
class Mailman21List extends MailinglistList {

  /**
   * ***************************************************************************
   * @name ctools_export_ui
   * @{
   */

  /**
   * Implements ctools_export_ui::edit_form().
   */
  function edit_form(&$form, &$form_state) {
dpm($form_state, 'Mailman21List::edit_form');

    // Must before call to parent to suppress the automatic default detection method
    $form['basic']['settings']['detect_method'] = array(
      '#type' => 'select',
      '#title' => t('Message Detection'),
      '#options' => array('rfc' => 'RFC-2369', 'generic' => 'Generic'),
      '#default_value' => $this->get_setting('detect_method'),
      '#description' => t('The method to use to detect messages from list'),
      '#required' => TRUE,
      '#weight' => 40,
    );

    parent::edit_form($form, $form_state);
dpm($form, 'Mailman21List::edit_form');

    if ($form['basic']['settings']['detect_method']['#default_value'] == 'rfc') {
      $form['basic']['settings']['list_id'] = array(
        '#type' => 'textfield',
        '#title' => t('List ID'),
        '#default_value' => $this->get_setting('list_id'),
        '#required' => TRUE,
        '#description' => t('The value if the List-ID header to check for'),
        '#weight' => 50,
      );
    }
  }
/// @}
  /**
   * ***************************************************************************
   * @name MailinglistListInterface
   *
   * @{
   */

  /**
   * processMessage()
   *
   * Implements MailinglistList::checkMessage().
   * @todo
   */
  function checkMessage(MailinglistMessageInterface $msg) {
    return TRUE;
  }

}
