<?php
/**
 * @file
 * Mailman21List class.
 * move to own module
 */

$plugin = array(
  'name' => 'Mailman 2.1 List',
  'description' => 'Mailman 2.1.x List Personality Module',
  'handler' => array(
    'class' => 'Mailman21List',
    'parent' => 'MailinglistList'
  ),
  'file' => 'Mailman21List.class.php',
);
