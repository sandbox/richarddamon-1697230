<?php
/**
 * @file
 * Mailinglist front end pages
 *
 */

/**
 * Page call back function for mailinglist
 *
 * address is of form
 * /mailinglist/listname/operation/
 * (mailinglist can be replaced with a different path)
 * if no listname, give general information with a list of mailing lists available
 * if no operation, give list introduction with a list of operations
 * operations looked up in via plugin.
 */
function _mailinglist_pagecallback() {
  $base = variable_get('mailinglist_base', 'mailinglist');
  $breadcrumb = array(
    l(t('Home'), '<front>'),
  );
  drupal_set_breadcrumb($breadcrumb);

  $lists = mailinglist_list_load_all(false);
  $page = array();
  if (empty($lists)) {
    $page['list'] = array('#markup' => 'No Mailinglist defined');
  }
  else {
    $page['head'] = array('#markup' => 'This is a listing of lists available');
    $page['list'] = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#attributes' => array('class' => 'mailinglist_list'),
    );
    foreach ($lists as $index => $list) {
      // only show pages the user has access to
      if (user_access('mailinglist ' . $index)) {
        $admin_name = check_plain($list->admin_name);
        $admin_title = check_plain($list->admin_title);
        $markup = "<a href=/$base/$admin_name>$admin_title</a>";
        $page['list']['#items'][$index] = array('data' => $markup);
      }
    }
  }
  return $page;
}

/**
 * _mailinglist_list_pagecallback()
 *
 * callback for $base/%mailinglist_list
 */
function _mailinglist_list_pagecallback($list = '', $oper = '', $p1 = '', $p2 = '',
  $p3 = '', $p4 = '') {
  $base = variable_get('mailinglist_base', 'mailinglist');
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Mailing Lists'), $base),
  );
  drupal_set_breadcrumb($breadcrumb);

  $page = array();
  if (!($list instanceof MailinglistListInterface) || !user_access('mailinglist ' . $list->admin_name)) {
    drupal_set_title(t('Unknown Mailing List'));
    $page = array("#markup" => "Unknown Mailing List.");
  }
  else {
    $listname = $list->admin_name;
    drupal_set_title($list->admin_title);
    $page['oper'] = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#attributes' => array('class' => 'mailinglist_list'),
    );
    foreach ($list->operations as $index => $enabled) {
      if ($enabled) {
        $oper = mailinglist_operation_load($index);
        if ($oper->hasPermission($list)) {
          $page['oper']['#items'][$index] = array('data' => l($oper->plugin['name'], "$base/$listname/$index"));
        }
      }
    }
  }
  return $page;
}

/**
 * mailinglit_operationr_form().
 *
 * Implements hook_form();
 */
function mailinglist_operation_form($form, &$form_state) {
  $list = $form_state['build_info']['args'][0];
  $operation = $form_state['build_info']['args'][1];

  if (!($list instanceof MailinglistListInterface)) {
    drupal_set_title(t('Unknown Mailing List'));
    $form = array("#markup" => "Unknown Mailing List.");
    return $form;
  }

  if (!($operation instanceof MailinglistOperationInterface) || !$operation->hasPermission($list)) {
    drupal_set_title(t('Unknown Operation'));
    $form = array("#markup" => "Unknown Operation.");
    return $form;
  }
  $base = variable_get('mailinglist_base', 'mailinglist');
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Mailing Lists'), $base),
    l($list->admin_title , $base . '/' . $list->admin_name),
  );
  drupal_set_breadcrumb($breadcrumb);
  drupal_set_title($list->admin_title . ' - ' . $operation->plugin['name']);

  $operation->form($form, $form_state, $list);

  return $form;
}

/**
 * mailinglit_operationr_form_validate().
 *
 * Implements hook_form_validate();
 */
function mailinglist_operation_form_validate($form, &$form_state) {
  $list = $form_state['build_info']['args'][0];
  $operation = $form_state['build_info']['args'][1];
  if ($operation instanceof MailinglistOperationInterface) {
    $operation->validate($form, $form_state, $list);
  }
  else {
    dpm(func_get_args(), 'validate?');
  }
}

/**
 * mailinglit_operationr_form_submit().
 *
 * Implements hook_form_submit();
 */
function mailinglist_operation_form_submit($form, &$form_state) {
  $list = $form_state['build_info']['args'][0];
  $operation = $form_state['build_info']['args'][1];
  if ($operation instanceof MailinglistOperationInterface) {
    $operation->submit($form, $form_state, $list);
  }
  else {
    dpm(func_get_args(), 'submit?');
  }
}

