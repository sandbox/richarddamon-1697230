<?php
/**
 * @file
 * MailinglistGenericList class.
 *
 */
/**
 * Generic List Pluging definition
 */

$plugin = array(
  'name' => 'Generic List',
  'description' => 'Generic List Personality Module',
  'handler' => array(
    'class' => 'MailinglistGenericList',
    'parent' => 'MailinglistList'
  ),
  'file' => 'MailinglistGenericList.class.php',
);
