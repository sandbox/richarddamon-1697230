<?php

/**
 * @file
 * Definition of MailinglistListInterface interface.
 * This class provides the interface between Mailinglist and the Mailinglist List personality modules
 */

/**
 * Interface to a mailing list.
 */
interface MailinglistListInterface {
  /**
   * @name MailinglistListInterface
   * @{
   */

  /**
   * checkMessage()
   *
   * Called on each message read from a mailbox to see if we want to process it.
   *
   * @param $message a MailinglistMessage object with the email received
   * @return TRUE if we have processed the message, FALSE if we don't know what to do.
   */
  function checkMessage(MailinglistMessageInterface $message);

  /**
   * processMessage()
   *
   * Processes the receipt of a message from the list.
   * It is expected that this will call hook_mailinglist_message_alter() and
   * hook_mailinglist_message().
   * MailinglistList::processMessage() does this for classes derived from it.
   *
   * Typically called by checkMessage, by may also be called by an importer that
   * knows what list a message is from (and might not pass the chekMessage test).
   *
   * @param $message a MailinglistMessage object with the email to process.
   *
   */
  function processMessage(MailinglistMessageInterface $message);

  /**
   * schema();
   *
   * Update schema for a list.
   *
   * @param $schema Array with schema for the list.
   *
   * @param $type The operation the schema is to support
   */
  function schema(&$schema, $type);
/// @}
};
