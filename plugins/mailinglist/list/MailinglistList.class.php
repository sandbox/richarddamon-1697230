<?php
/**
 * @file
 * Definition of MailinglistList class.
 * This class provides the interface between Mailinglist and the Mailinglist List personality modules
 */

/**
 * @page mailinglist-plugin-list
 *
 * @subpage mailinglist-plugin-list-generic
 * @subpage mailinglist-plugin-list-mailman21
 */

/**
 * Retrieve messages from a Mailhandler Mailbox.
 */
abstract class MailinglistList extends MailinglistExportable implements MailinglistListInterface {
  var $operations;
  var $parameters;

  /**
   * ***************************************************************************
   * @name MailinglistListInterface
   * @{
   */

  /**
   * Called on each message read from a mailbox to see if we want to process it.
   *
   * @param $msg an Mailinglist_Message object with the email received
   * @return TRUE if we have processed the message, FALSE if we don't know what to do.
   */
  function checkMessage(MailinglistMessageInterface $msg) {
    return FALSE;
  }

  /**
   * generic_list_match()
   *
   * Performs the generic list matching processes
   */
  function genericListMatch(MailinglistMessageInterface $msg) {
    if (!isset($msg->headers[$this->get_setting('generic_detect_header')])) return FALSE;
    $header = $msg->headers[$this->get_setting('generic_detect_header')];
    dpm($header, 'generic_list_match');
    return TRUE; ///@todo
  }

  /**
   * message_process()
   *
   * Processes the receipt of a message from the list.
   * calls hook_mailinglist_message();
   */
  function processMessage(MailinglistMessageInterface $message) {
    drupal_alter('mailinglist_message', $message, $this);
    module_invoke_all('mailinglist_message', $message, $this);
  }

  /**
   * schema()
   */
  function schema(&$schema, $type) {
  }

/// @}

  /**
   * ***************************************************************************
   * @name ctools_export_ui
   * @{
   */

  /**
   * Implements ctools_export_ui::edit_form(),
   * cross linked via mailinglist_export_ui.class.php
   *
   * This hook is responsible for building the edit/create form for the object.
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);

    /* Administrative Title for the list extry (builds default list_name); */
    $form['info']['admin_title']['#description'] = t('Suggested, but not required, to be the email address of the maiinglist.');

    $form['basic']['settings']['to_address'] = array(
      '#type' => 'textfield',
      '#title' => t('To Address'),
      '#default_value' => $this->get_setting('to_address'),
      '#required' => TRUE,
      '#description' => t('The Email address to send messages to the list'),
      '#weight' => 10,
    );

    $form['basic']['settings']['from_address'] = array(
      '#type' => 'textfield',
      '#title' => t('From Address'),
      '#default_value' => $this->get_setting('from_address'),
      '#required' => TRUE,
      '#description' => t('The default From address the site should use to send Email to the list'),
      '#weight' => 20,
    );

    if (!isset($form['basic']['settings']['detect_method'])) {
      // No Detectect method defined, so add default.
      $form['basic']['settings']['detect_method'] = array(
        '#type' => 'hidden',
        '#value' => 'generic',
      );
    }

    // Remember the method used to build the form.
    $form['basic']['settings']['detect_method_menu'] = array(
      '#type' => 'hidden',
      '#value' => isset($form['basic']['settings']['detect_method']['#default_value']) ? $form['basic']['settings']['detect_method']['#default_value'] : $form['basic']['settings']['detect_method']['#value']
    );

    if ($form['basic']['settings']['detect_method_menu']['#value'] == 'generic') {
      $form['basic']['settings']['generic_detect_header'] = array(
        '#type' => 'textfield',
        '#title' => t('Header'),
        '#default_value' => $this->get_setting('generic_detect_header'),
        '#required' => TRUE,
        '#description' => t('The Header to check to see if message comes from the list'),
        '#weight' => 50,
      );
      $form['basic']['settings']['generic_detect_regex'] = array(
        '#type' => 'textfield',
        '#title' => t('Regex'),
        '#default_value' => $this->get_setting('generic_detect_regex'),
        '#required' => TRUE,
        '#description' => t('Regex to test Header with'),
        '#weight' => 60,
      );
    }
    $form['operations'] = array(
      '#tree' => FALSE,
      '#weight' => 50,
      'operations' => array(
        '#type' => 'fieldset',
        '#title' => 'Enabled Operations',
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ),
    );
    $opers = mailinglist_operation_load_all();
    foreach ($opers as $key => $oper) {
      /// @todo Should this be conditioned on operation being available for the list
      /// or will the fact that this might depend on settings give us problems.
      $form['operations']['operations'][$key] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($oper->plugin['name']),
        '#default_value' => isset($this->operations[$key]) && !empty($this->operations[$key]),
      );
    }
    dpm($form, 'MailinglistList::edit_form form');
    dpm($form_state, 'MailinglistList::edit_form form_state');
  }

  /**
   * edit_form_validate()
   *
   * Implements ctools_export_ui::edit_form_validate().
   */
  function edit_form_validate(&$form, &$form_state) {
    parent::edit_form_validate($form, $form_state);

    if (!valid_email_address($form_state['input']['settings']['to_address'])) {
      form_set_error('to_address', 'To email address is not valid');
    }
    if (!valid_email_address($form_state['input']['settings']['from_address'])) {
      form_set_error('from_address', 'From email address is not valid');
    }
    if ($form_state['input']['settings']['detect_method'] != $form_state['input']['settings']['detect_method_menu']) {
      form_set_error('detect_method', 'Type Changed, Check Settings!');
    }
  }
  /// @}
}
