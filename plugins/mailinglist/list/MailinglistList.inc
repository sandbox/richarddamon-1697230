<?php
/**
 * @file
 * MailinglistRetrieve class.
 */

$plugin = array(
  'hidden' => TRUE,
  'handler' => array(
    'class' => 'MailinglistList',
    'parent' => 'MailinglistExportable',
  ),
  'file' => 'MailinglistList.class.php',
);
