<?php
/**
 * @file
 * Definition of MailinglistGenericList class.
 *
 * @page mailinglist-plugin-list-generic
 */


/**
 * @page mailinglist-plugin-list-generic
 */
/**
 * Generic List Personality Plugin.
 */
class MailinglistGenericList extends MailinglistList {
  /**
   * @name ctools_export_ui
   * @{
   */
  /**
   * Implements ctools_export_ui::edit_form().
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);
  }

  /// @}
  /**
   * @name MailinglistListInterface
   * @{
   */

  /**
    * checkMessage()
    *
    * Overrides/Implements MailinglistList::processMessage().
    */
  function checkMessage(MailinglistMessageInterface $msg) {
    if ($this->generic_list_match($msg)) {
      $this->processMessage($msg);
      return TRUE;
    }
    return FALSE;
  }
  /// @}
}
