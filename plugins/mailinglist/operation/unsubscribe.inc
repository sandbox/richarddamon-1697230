<?php
/**
 * @file
 * MailinglistModule class.
 */

$plugin = array(
  'name' => 'Unsubscribe',
  'description' => 'Provides a page to subscribe to a list',
  'handler' => array(
    'class' => 'MailinglistUnsubscribeOperation',
    'parent' => 'MailinglistOperation',
  ),
  'file' => 'MailinglistUnsubscribeOperation.class.php',
);
