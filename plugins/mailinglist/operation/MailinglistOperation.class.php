<?php
/**
 * @file
 * Definition of MailinglistOperation class.
 */

/**
 * Base Class for Mailinglist extensions
 */
abstract class MailinglistOperation implements MailinglistOperationInterface {
/**
 * @name MailinglistOperationInterface
 *
 * @{
 */
  /**
   * Implements MailinglistOperationInterface::isAvailable().
   */
  function isAvailable(MailinglistListInterface $list) {
    return TRUE;
  }

  /**
   * Implements MailinglistOperationInterface::hasPermission().
   */
  function hasPermission(MailinglistListInterface $list) {
    return user_access('mailinglist ' . $list->admin_name . ' ' . $this->plugin['key']);
  }

/**
 * Operation page form generation
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function form(&$form, &$form_state, $list) {}

/**
 * Operation page form validation
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function validate(&$form, &$form_state, $list) {}

/**
 * Operation page form submission
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function submit(&$form, &$form_state, $list) {}

/// @}
}
