<?php
/**
 * @file
 * MailinglistModule class.
 */

$plugin = array(
  'name' => 'Subscribe',
  'description' => 'Provides a page to subscribe to a list',
  'handler' => array(
    'class' => 'MailinglistSubscribeOperation',
    'parent' => 'MailinglistOperation',
  ),
  'file' => 'MailinglistSubscribeOperation.class.php',
);
