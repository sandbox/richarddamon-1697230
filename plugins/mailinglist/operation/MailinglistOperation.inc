<?php
/**
 * @file
 * MailinglistModule class.
 */

$plugin = array(
  'hidden' => TRUE,
  'handler' => array(
    'class' => 'MailinglistOperation',
  ),
  'file' => 'MailinglistOperation.class.php',
);
