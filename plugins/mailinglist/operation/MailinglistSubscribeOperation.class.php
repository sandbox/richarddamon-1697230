<?php
/**
 * @file
 * Definition of MailinglistOperation class.
 */

/**
 * Base Class for Mailinglist extensions
 */
class MailinglistSubscribeOperation extends MailinglistOperation {

/**
 * @name MailinglistOperationInterface
 *
 * @{
 */

/**
 * Operation page form generation
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function form(&$form, &$form_state, $list) {}

/**
 * Operation page form validation
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function validate(&$form, &$form_state, $list) {}

/**
 * Operation page form submission
 *
 * @param $form Form for the page.
 * @param $form_state Form state for the page.
 * @param $list List we are generating the page for.
 */
  function submit(&$form, &$form_state, $list) {}

/// @}
}
