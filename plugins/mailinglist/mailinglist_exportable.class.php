<?php
/**
 * @file
 * Definition of mailinglist_exportable class.
 */

/**
 * Place for common code for classes used in export_ui classes
 *
 * Main purpose is to make sure all export_ui classes implement
 * the hooks forwarded by the export_ui code
 */
abstract class MailinglistExportable implements MailinglistPolymorphicExportableInterface {
  public $object_type;  ///< The type of object we are
  public $settings;     ///< Generic Array to hold misc settings so all can use same schema.
  public $export_type;  ///< Needed by CTools

  /**
   * get_settings()
   *
   * Get a settings from the object.
   *
   * @param $name The name of the setting.
   * @param $def  Default value to return if setting is not present.
   */
  public function get_setting($name, $def=NULL) {
    if (isset($this->settings[$name])) return $this->settings[$name];
    return $def;
  }

  /******************** MailinglistPolymorphicExportableInterface *************/
  /**
   * @name MailinglistPolymorphicExportableInterface
   * @{
   */

  /**
   * created()
   *
   * hook called when a new object has been created in the database.
   */
  function created() {
    $ret = module_invoke_all($this->baseName() . '_created', $this);
  }

  /**
   * edited()
   *
   * hook called when an object has been updated in the database.
   */
  function edited(MailinglistPolymorpicExportableInterface $old) {
    $ret = module_invoke_all($this->baseName() . '_edited', $this, $old);
  }

  /**
   * changedType()
   *
   * hook called when an object has had its object_type changed in the database.
   */
  function changedType(MailinglistPolymorpicExportableInterface $old) {
    $ret = module_invoke_all($this->baseName() . '_changed_type', $this, $old);
  }

  /**
   * delete()
   *
   * hook called when an object is about to be deleted from the database.
   */
  function delete() {
    dpm($this, 'exportable::delete');
    $ret = module_invoke_all($this->baseName() . '_delete', $this);
  }

  /**
   * baseName()
   *
   * @returns string, the base name for hooks, default is the database table name
   */
  function baseName() {
      return $this->table;
  }

/// @}

  /******************** ctools_export_ui interface ****************************/
  /**
   * @name ctools_export_ui_interface
   * @{
   */

  /**
   * Implements ctools_export_ui::edit_form()
   * called via mailinglist_export_ui
   * Build an edit form for our object
   */
  public function edit_form(&$form, &$form_state) {
  }

  /**
   * Implements ctools_export_ui::edit_form_validate().
   * called via mailinglist_export_ui
   */
  function edit_form_validate(&$form, &$form_state) {
  }
/// @}

  /**
   * @name Logging
   * @{
   */
/**** Loging Interface ******/

  const LOG_OFF = 0;
  const LOG_SCREEN = 1;
  const LOG_DB = 2;
  const LOG_DB_SCREEN = 3;
  const LOG_FILE = 4;
  const LOG_FILE_SCREEN = 5;

  const LOG_NOTICE = WATCHDOG_NOTICE;
  const LOG_WARNING = WATCHDOG_WARNING;
  const LOG_ERROR = WATCHDOG_ERROR;

  public $log_normal = self::LOG_OFF;      ///< Should we log messages?
  public $log_error =  self::LOG_DB_SCREEN;

  /**
   * log()
   */
  public function log($str, $parms = array(), $level = self::LOG_NOTICE) {
    $this->log_message($this->log_normal, $str, $parms, $level);
  }

  /**
   * log_error()
   *
   * Log an error condition
   */
  public function log_error($str, $parms = array(), $level = self::LOG_ERROR) {
    $this->log_message($this->log_error, $str, $parms, $level);
  }

  /**
   * log_message()
   *
   * Log a message
   *
   */
  public function log_message($flags, $str, $parms, $level) {
    if ($flags & self::LOG_DB) {
      $this->log_db($str, $parms, $level);
    }
    if ($flags & self::LOG_FILE) {
      $this->log_file($str, $parms, $level);
    }
    if ($flags & self::LOG_SCREEN) {
      $this->log_screen($str, $parms, $level);
    }
  }

  /*****************************************************************************
  These methods provide implementations for the various logging options above,
  while they can be directly called, it is not normal.
  */

  /**
   * log_screen()
   *
   * log a message to the screen
   */
  public function log_screen($str, $parms = array(), $level = self::LOG_NOTICE) {
    static $levels = array(
      self::LOG_NOTICE => 'status',
      self::LOG_WARNING => 'warning',
      self::LOG_ERROR => 'error');
    ///@todo Need to do substitution into $str
    dpm($str, $this->object_type, $levels[$level]);
  }


  /**
   * log_db().
   *
   * Log a message to the watchdog Database logs.
   */
    public function log_db($str, $parms = array(), $level = self::LOG_ERROR) {
    watchdog($this->object_type, $str, $parms, $level);
  }

  /**
   * log_file().
   *
   * Log a message to a logfile.
   * @todo
   */
  public function log_file($str, $parms = array(), $level = self::LOG_INFO) {
  }

 /// @}
};


