<?php
/**
 * @file
 * Interface for defining an Email Message for the Mailinglist Module
 */

/**
 * Interface MailinglistMessageInterface
 *
 */
interface MailinglistMessageInterface {
  function __construct($msg=NULL);
  function header($name);
};
