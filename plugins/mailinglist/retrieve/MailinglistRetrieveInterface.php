<?php
/**
 * @file
 * Definition of MailinglistRetrieveInterface.
 */

/**
 * interface MailinglistRetrieveInterface
 */
interface MailinglistRetrieveInterface {
  /**
   * @name MailinglistRetrieveInterface
   *
   * @{
   */


  /**
   * Test connection.
   */
  public function test();
  /**
   * Returns a list of messages that are available
   */
  public function get_message_list($max=0);

  /** Get an email message by its id code (what is returned by get_message_list() */
  public function get_message($id);

  /** mark message for deletion */
  public function purge_message($id);

  /** delete marked messages and close connection */
  public function close();
  /// @}
};
