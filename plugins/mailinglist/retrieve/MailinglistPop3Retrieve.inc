<?php
/**
 * @file
 * MailinglistPop3Retrieve class.
 */

$plugin = array(
  'name' => 'Mailinglist Pop3',
  'description' => 'Retrieves mail from a Pop3 mailbox.',
  'handler' => array(
    'class' => 'MailinglistPop3Retrieve',
    'parent' => 'MailinglistRetrieve'
  ),
  'file' => 'MailinglistPop3Retrieve.class.php',
);
