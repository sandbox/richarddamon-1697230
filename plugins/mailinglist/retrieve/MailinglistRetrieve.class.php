<?php
/**
 * @file
 * Definition of MailinglistRetrieve class.
 *
 * @page mailinglist-plugin-retreive
 */

require_once("MailinglistMessage.class.php");

/**
 * Retrieve messages from a Mailinglist Mailbox.
 */
abstract class MailinglistRetrieve extends MailinglistExportable implements MailinglistRetrieveInterface {
  /**
   * @name MailinglistRetrieveInteface
   *
   * @{
   */

  /**
   * Test connection.
   */
  public function test() {
    return array();
  }

  /**
   * Returns a list of messages that are available
   */
  public function get_message_list($max=0) {
    return array();
  }

  /** Get an email message by its id code (what is returned by get_message_list() */
  public function get_message($id) {
    return FALSE;
  }

  /** mark message for deletion */
  public function purge_message($id) {
    return;
  }

  /** delete marked messages and close connection */
  public function close() {
  }
  /// @}
  /**
  * @name ctools_export_ui
  *
  * @{
  */
  /**
   * Implements ctools_export_ui::edit_form().
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);
    $form['info']['admin_title']['#description'] = t('Suggested, but not required, to be the email address of the mailbox.');

    $form['extra']['settings']['limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum messages to retrieve'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->get_setting('limit', 0),
      '#description' => t('To prevent timeout errors from large mailboxes you can limit the maximum number of messages that will be retrieved during each cron run. Set to zero for no limit.'),
      '#element_validate' => array('element_validate_integer'),
      '#required' => TRUE,
  );
    $form['extra']['settings']['encoding'] = array(
      '#type' => 'textfield',
      '#title' => t('Default character encoding'),
      '#default_value' => $this->get_setting('encoding', 'UTF-8'),
      '#description' => t('The default character encoding to use when an incoming message does not define an encoding.'),
      '#required' => TRUE,
  );
  }

  /**
   * Implements ctools_export_ui::edit_form_validate().
   */
  function edit_form_validate(&$form, &$form_state) {
    parent::edit_form_validate($form, $form_state);
    /// @todo _mailinglist_list_test($form, $form_state);
  }

  /**
   * Implements ctools_export_ui::edit_form_submit().
   */
  function edit_form_submit(&$form, &$form_state) {
    parent::edit_form_submit($form, $form_state);
  }
  /// @}
}
