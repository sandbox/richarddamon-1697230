<?php
/**
 * @file
 * Definition of MailinglistPop3Retrieve class.
 */

/**
 * Retrieve messages using PHP IMAP library.
 */
class MailinglistPop3Retrieve extends MailinglistRetrieve {

  /**
   * @name ctools_export_ui
   *
   * @{
   */
  /**
   * Implements ctools_export_ui::edit_form().
   * called via mailinglist_export_ui
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);
    global $cookie_domain;

    /* Parameters that we need:
    Base:
    Hostname:
    Port:
    Timeout:
    UseSocket
    Username:
    Password:

    Extra:
    APOPDetect:
    Delete when done:
    Logging: No, Yes, Yes-ShowPasswords
    */

    $ajax_settings = array(
      'callback' => '_mailinglist_mailbox_test',
      'wrapper' => 'mailinglist_test_results',
      'event' => 'change',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Please wait - testing connection settings...'),
      ),
    );
    $form['connection']['settings']['domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Domain'),
      '#default_value' => $this->get_setting('domain'),
      '#required' => TRUE,
      '#description' => t('The domain of the server used to collect mail.'),
      '#ajax' => $ajax_settings,
    );
    $form['connection']['settings']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->get_setting('port'),
      '#description' => t('The mailbox port number (usually 110 for POP3).'),
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
      '#ajax' => $ajax_settings,
    );
    $form['connection']['settings']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->get_setting('name'),
      '#required' => TRUE,
      '#description' => t('This username is used while logging into this mailbox during mail retrieval.'),
      '#ajax' => $ajax_settings,
    );
    $form['connection']['settings']['pass'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => _mailinglist_decode_password($this->get_setting('pass')),
      '#description' => t('The mailbox password corresponding to the username above. Consider using a non-vital password, since this field is stored with minimal encryption in the database and displayed here.'),
      '#ajax' => $ajax_settings,
    );

    $form['connection']['settings']['results'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'mailinglist_test_results',
      ),
    );

    $form['connection']['settings']['timeout'] = array(
      '#type' => 'textfield',
      '#title' => t('Timeout'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->get_setting('timeout', 10),
      '#description' => t('Timeout (in Sec) to connect to server'),
      '#element_validate' => array('element_validate_number'),
      '#required' => TRUE,
      '#ajax' => $ajax_settings,
    );

    $form['extra']['settings']['apop'] = array(
      '#type' => 'checkbox',
      '#title' => t('Detect APOP?'),
      '#default_value' => $this->get_setting('apop', FALSE),
      '#description' => t('Check to enable APOP protocal detection'),
    );

    $form['extra']['settings']['delete_after_read'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete messages after they are processed?'),
      '#default_value' => $this->get_setting('delete_after_read', TRUE),
      '#description' => t('Uncheck this box to leave read messages in the mailbox. They will not be processed again unless they become marked as unread. You normallys should check this box.'),
    );

    $form['extra']['settings']['ipv6'] = array(
      '#type' => 'checkbox',
      '#title' => t('IPv6'),
      '#default_value' => $this->get_setting('ipv6', FALSE),
      '#description' => t('Use IPv6'),
    );

  }

  /**
   * Implements ctools_export_ui::edit_form_validate().
   * Called via mailinglist_export_ui.
   */
  function edit_form_validate(&$form, &$form_state) {
    parent::edit_form_validate($form, $form_state);
    if ($form_state['values']['settings']['port'] < 1 || 65535 < $form_state['values']['settings']['port']) {
      form_set_error('port', t('Port must be between 1 and 65535'));
    }

    if ($form_state['values']['settings']['port'] != 110) {
      drupal_set_message(t('Non-standard Pop3 Port: @port', array('@port' => (int)$form_state['values']['settings']['port'])), 'warning');
    }

    // If POP3 mailbox is chosen, messages should be deleted after processing.
    // Do not set an actual error because this is helpful for testing purposes.
    if ( $form_state['values']['settings']['delete_after_read'] == 0) {
      drupal_set_message(t('Unless you check off "Delete messages after they are processed" when using a POP3 mailbox, old emails will be re-imported each time the mailbox is processed.'), 'warning');
    }
  }

  /**
   * Implements ctools_export_ui::edit_form_submit().
   * Called via mailinglist_export_ui.
   */
  function edit_form_submit(&$form, &$form_state) {
    parent::edit_form_submit($form, $form_state);
  }
  /// @}


  /**
   * @name MailinglistRetrieveInterface
   *
   * @{
   */

  /**
   * Overrides/Implements Mailinglist:Retrieve::get_message_list
   * Connect to mailbox and run message retrieval.
   *
   * @return array
   *   Retrieved messages.
   */
  public function get_message_list($max=0) {
    if (!$this->connected) {
      if (!$this->connect()) return FALSE;
    }

    $stat = $this->get_stat();
    $limit = $stat['count'];
    if ($max > 0 && $max < $limit) $limit = $max;
    $res = array();
    for ($i=1; $i<=$limit; $i++) {
      $res[] = $i;
    }
    return $res;
  }

  /**
   * Overrides/Implements MailinglistRetrieve::get_message().
   */
  public function get_message($id) {
    $msg = $this->get_msg($id);
    return new MailinglistMessage($msg);
  }

  /**
   * Overrides/Implements MailinglistRetrieve::purge_message();
   */
  public function purge_message($id) {
    $this->delete_msg($id);

  }

  /**
   * Overrides/Implements MailinglistRetrieve::close().
   */
  public function close() {
    $this->quit();
    $this->disconnect();
  }
  /// @}

  /*****************************************************************************
   * Private implementation
   */

  const DEFAULT_BUFFER_SIZE = 4096;

  private $connected = FALSE;   ///< Are we currently connected/logged in
  private $socket;              ///< Handle for Socket being used
  private $ipaddr;              ///< String for ipaddr we are connecting to
  private $apop_banner = NULL;  ///< Banner for APOP

  /**
   * Destructor
   *
   */
  public function __destruct() {
    $this->disconnect();
  }

  /**
   * connect()
   *
   * Build connection/log in to Pop3 server.
   *
   */
  protected function connect() {
    // Make sure all needed parms are defined so we don't need to test everywhere
    if (!isset($this->settings['apop'])) {
      $this->settings['apop'] = FALSE;
    }

    if (!isset($this->settings['use_sockets']) || !extension_loaded("sockets")) {
      $this->settings['use_sockets'] = FALSE;
    }

    if (!isset($this->settings['ipv6'])) $this->settings['ipv6'] = FALSE;

    $timeout = (double)$this->settings['timeout'];
    $timeout_sec = (int)floor($timeout);
    $timeout_usec = (int)(1000000*($timeout-$timeout_sec));
    $timeout_array = array('sec' => $timeout_sec, 'usec' => $timeout_usec);
    $host = $this->settings['domain'];
    $port = $this->settings['port'];

    if ($this->settings['use_sockets']) {
      $this->socket = socket_create(($this->settings['ipv6'] ? AF_INET6 : AF_INET), SOCK_STREAM, SOL_TCP );
      if (!$this->socket) {
        $this->log_error('Error creating Socket');
        return;
      }
      if (!socket_set_option($this->socket, SOL_SOCKET, SO_RCVTIMEO, $timeout_array) || !socket_set_option($this->socket, SOL_SOCKET, SO_SNDTIMEO, $timeout_array)) {
        $this->log_error('Error setting socket timeout');
        return FALSE;
      }
      if (!socket_connect($this->socket, $host, $port) || !socket_getpeername($this->socket, $this->ipaddr)) {
        $this->log_error("Error connecting to server");
        return FALSE;
      }
    }
    else {
      $url = 'tcp://' . $host . ':' . $port;
      $intErrno = 0;
      $strError = "";
      $this->socket = fsockopen($url, $intErrno, $strError, $timeout);
      if (!stream_set_timeout($this->socket, $timeout_sec, $timeout_usec)) {
        $this->log_error('Error setting socket timeout');
      }
      $this->ipaddr = gethostbyname($host);
    }
    $this->log("Connected to " . $this->ipaddr . ':' . $port . ' [' . $host . ']');

    // Get the first response with, if APOP support avalible, the apop banner.
    $buff = "";
    $buff = $this->recv_string();

    // Check for APOP banner
    $start = strpos($buff, '<');
    if ($start !== FALSE) {
      $stop = strpos($buff, '>', $start);
      if ($stop != FALSE) {
        $this->apop_banner = substr($buff, $start+1, $stop-$start-1);
      }
    }
    // Now we can Login to the server
    $apop = FALSE;

    if ($this->settings['apop'] && !is_null($this->apop_banner)) {
      $apop = TRUE;
    }

    if ($apop) {
      // APOP Auth
      $ans = $this->send_cmd("APOP " . $this->settings['name'] . " " . hash("md5", $this->strAPOPBanner . _mailinglist_decode_password($this->settings['pass']), FALSE));
    }
    else {
      // POP3 Auth
      $this->send_cmd( "USER " . $this->settings['name']);
      $ans = $this->send_cmd( "PASS " . _mailinglist_decode_password($this->settings['pass']));
    }
    if ($ans[0] == '+') {
      $this->connected = TRUE;
    }
    return $this->connected;
  }

  /*
     * Disconnect from the server.
     * CAUTION:
     * This function doesn't send the QUIT command to the server so all as delete marked emails won't delete.
     *
     * @return void
  */
  protected function disconnect() {
    if ($this->connected ) {
      if ($this->settings['use_sockets']) {
        if (socket_close($this->socket) === FALSE ) {
          $this->log_error("Error On Socket Disconnect");
        }
      }
      else {
        if (!fclose($this->socket)) {
          $this->log_error("Error On Socket Disconnect");
        }
      }
      $this->connected = FALSE;
    }
  }

  /**
   * Get the office status. That means that you will get an array
   * with all needed informations about your mail drop.
   * The array is build up like discribed here.
   *
   * $result = array( "count" => "Count of messages in your mail drop",
   *                  "octets" => "Size of your mail drop in bytes",
   *
   *                  "msg_number" => array("uid" => "The unique id of the message on the pop3 server",
   *                                        "octets" => "The size of the message in bytes"
   *                                  ),
   *                  "and soon"
   *          );
   *
   * @return array
   */
  protected function getOfficeStatus($limit = 0) {
    if (!$this->connected) {
      $this->connect();
    }
    $res = array();

    $res = $this->get_stat();

    if ($res["count"] > 0) {
      $uidls = $this->get_uidls();
      $lists = $this->get_list();

      if ($limit == 0 || $limit > $res['count']) $limit = $res['count'];
      for ($i=1; $i<=$limit; $i++) {
        list(, $uidl) = explode(" ", trim($uidls[$i-1]));
        list(, $list) = explode(" ", trim($lists[$i-1]));
        $res[$i]["uid"] = (int) $uidl;
        $res[$i]["octets"] = (int) $list;
      }
    }
    return $res;
  }

/**
 * get_uidls()
 *
 * Get list of message uidls
 *
 */
  protected function get_uidls() {
    if (!$this->connected) {
      $this->connect();
    }
    $this->send_cmd("UIDL");
    $uidls = $this->recv_to_point();
    $uidls = explode("\r\n", trim($uidls));
    return $uidls;
  }

/**
 * get_list()
 *
 * Get list of messages on server, and their size
 */
  protected function get_list() {
    if (!$this->connected) {
      $this->connect();
    }
    $this->send_cmd("LIST");
    $lists = $this->recv_to_point();
    $lists = explode("\r\n", trim($lists));
    return $lists;
  }

  /**
   * Get the stats from the pop3 server
   * This is only a string with the count of mails and their size in your mail drop.
   *
   * @return string  example: "+OK 2 3467"
   * @throw POP3_Exception
   */
  protected function get_stat() {
    if (!$this->connected) {
      $this->connect();
    }
    $stat = $this->send_cmd("STAT");
    $stat = explode(" ", trim($stat));
    $res['count'] = (int) $stat[1];
    $res['octets'] = (int) $stat[2];
    return $res;
  }

  /*
     * Mark a message as delete
     *
     * @param int $msg_num  Message Number on the pop3 server
     *
     * @return NULL
     * @throw POP3_Exception
  */
  protected function delete_msg( $msg_num ) {
    $this->send_cmd("DELE " . $msg_num);
    return;
  }

  /**
   * Send the quit command to the server.
   * All as delete marked messages will remove from the mail drop.
   *
   */
  protected function quit() {
    if ($this->connected) {
      $this->send_cmd("QUIT");
    }
  }

  /**
   * Recieve a raw message.
   *
   * @param $msg_num  The message number on the pop3 server.
   *
   * @return string  Complete message
   */
  protected function get_msg( $msg_num ) {
    $this->send_cmd("RETR " . $msg_num);
    return $this->recv_to_point();
  }

  /**
   * recv_string
   *
   * Receive a string from the socket.
   *
   * @param $buff_size [int] the size of the buffer to read.
   * @param $log [bool] should the data received be logged.
   */
  protected function recv_string($buff_size = self::DEFAULT_BUFFER_SIZE, $log = TRUE) {
    $buff = "";
    if ($this->settings['use_sockets']) {
      $buff = socket_read($this->socket, $buff_size , PHP_NORMAL_READ);
      if ($buff === FALSE ) {
        $this->log_error("Socket Error");
      }
      $len = strlen($buff);
      if ($buff[$len-1] != "\r") {
        $this->log_error('Line too long');
      }
      else {
        $buff = substr($buff, 0, $len-1);
        // Workaround because socket_read with PHP_NORMAL_READ stop at "\r" but the network string ends with "\r\n"
        // so we need to call the socket_read function again to get the "\n"
        $buff2 = socket_read($this->socket, 1 , PHP_NORMAL_READ);
        if ($buff2 === FALSE ) {
          $this->log_error("Socket Error");
        }
        if ($buff2 != "\n") {
          $this->log_error('Bad Line Ends');
        }
      }
    }
    else {
      $buff = fgets($this->socket, $buff_size);
      if (!$buff) {
        $this->log_error("fgets(): Couldn't recieve the string from socket");
      }
      $buff = substr($buff, 0, strlen($buff)-2);
    }
    if ($log) $this->log($buff);
    return $buff;
  }

  /**
   * recv_to_point
   *
   * Read from socket until we reach a line with just a period on it.
   * Return all the lines (but the .) with new-line between each.
   */
  protected function recv_to_point() {
    $buff = "";
    while (TRUE) {
      $buffer = $this->recv_string(self::DEFAULT_BUFFER_SIZE, FALSE);
      if ($buffer == '.') {
        break;
      }
      $buff .= $buffer . "\n";
    }
    return $buff;
  }

  /**
   * Send a string to the server.
   * Will append the network lineend "\r\n".
   *
   * @param $cmd [string] The string that should send to the pop3 server
   *
   * @return void
   */
  protected function send( $cmd ) {
    $cmd .= "\r\n";
    if ($this->settings['use_sockets']) {
      if (socket_send($this->socket, $cmd, strlen($cmd), 0) === FALSE ) {
        $this->log_error("Pop3 Send Error");
      }
    }
    else {
      if (!fwrite($this->socket, $cmd, strlen($cmd))) {
        $this->log_error("fwrite(): Failed to write string to socket");
      }
    }
  }

  /**
   * This function send the command to the server and will get the response
   * If the command goes failed, the function will throw the POP3_Exception with the
   * ERR_SEND_CMD error code and the response as error message.
   *
   * @param $cmd [string] The string with the command for the pop3 server
   *
   * @return [string]  Server response if it was successfull
   */
  protected function send_cmd($cmd) {
    $this->log($cmd);
    $this->send($cmd);
    $res = $this->recv_string();
    // 1. the check for the strlen of the result is a workaround for some server who don't send something after the quit command
    // 2. should run with qmailer too...qmailer bug (pop3.class.inc) "." instead of "+OK" after RETR command
    if (strlen($res) > 0 && $res[0] == '-') {
      $this->log_error("Response Error " . $res);
    }
    return $res;
  }
}
