<?php
/**
 * @file
 * Definition of MailinglistMessage (and related) classes
 *
 * References:
 * RFC5322 (which superceeds RFC822 and RFC2822) EMail Format
 * RFC2045, RFC2046, RFC2047, RFC2048, RFC2049 MIME Format
 * RFC6657 Update charset
 *
 */

 /**
  * class MailinglistHeader
  *
  * a helper class for parsing EMail Headers
  *
  * todo update nameing conventions.
  */
class MailinglistHeader{
  private $header;    ///< The header being processed

  /**
   * __construct()
   *
   * @param $str String with value of header to process
   */
  function __construct($str) {
    $this->header = $str;
  }

  /**
   * get_atom()
   *
   * Get an atom.
   */
  function get_atom() {
    static $atext = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&'*+-/=?^_`{}|~";
    $this->skip_ws();
    return $this->get_chunk($atext);
  }

  /**
   * get_dot_atom()
   *
   * get a dotted atom.
   */
  function get_dot_atom() {
    $atom = $this->get_atom();
    while ($this->header[0] == '.') {
      $atom += $this->get_char();
      $atom += $this->get_atom();
    }
    return $atom;
  }

  /**
   * get_token()
   *
   */
  function get_token() {
    static $ttext = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&'*+-^_`{}|~.";
    $this->skip_ws();
    return $this->get_chunk($ttext);
  }
  /**
   * get_value()
   *
   * Get a value ( token / qstring )
   */
  function get_value() {
    $this->skip_ws();
    if ($this->header[0] == '"') {
      $token = "";
      $this->get_char();
      while (($char = $this->get_char()) != '"' && $char !== FALSE) {
        if ($char == '\\') {
          $char = $this->get_char();
        }
        $token .= $char;
      }
      return $token;
    }
    else{
      return $this->get_token();
    }
  }

  /**
   * get_chunk()
   *
   * get a chunck of characters belonging to a class
   */
  private function get_chunk($chars) {
    $token = "";
    while (!empty($this->header) && strpos($chars, $this->header[0]) !== FALSE) {
      $token .= $this->get_char();
    }
    return $token;
  }
  /**
   * Skip White Space
   *
   * Production Name: FWS, *WSP
   */
  function skip_ws() {
    while (ctype_space($this->header[0])) {
      $this->header = substr($this->header, 1);
    }
  }

  /**
   * Skip Comments or whitespace
   *
   * Production name: CFWS
   */
  function skip_cws() {
    while (ctype_space($char = $this->header[0]) || $char == '(') {
      $this->get_char();
      if ($char == '(') {
        $this->skip_comment();
      }
    }
  }

  /**
   * Skip Comment.
   *
   * Assumes the leading ( has already been removed by production calling us.
   *
   * Production Name: comment
   */
  function skip_comment() {
    while (($char = $this->get_char()) != ')') {
      if ($char == '\\') {
        $this->get_char();  // Quoted char, so ignore the next
      }
      elseif ($char == '(') {
        $this->skip_comment();  // Nested comment
      }
    }
  }

  /**
   * get_char()
   *
   * Get a character from the header being processed
   */
  function get_char() {
    if (!empty($this->header)) {
      $char = $this->header[0];
      $this->header = substr($this->header, 1);
      return $char;
    }
    else {
      return FALSE;
    }
  }
  /**
   * push_back()
   *
   * push a character/string back into header
   */
  function push_back($char) {
    $this->header = $char . $this->header;
  }
}

 /**
  * MailinglistMessagePart
  * A class to represent a piece of a email message
  */
class MailinglistMessagePart {
  /** An array of the headers of the message part
   *  Header name is the index to the array.
   *  If header occures multiple times, then entry is an array of the occurances of it.
   */
  protected $headers;

  /**
   *  The body of the message,
   *  if it is a multi-part then it is an array of the various multi-parts.
   *  if it is a message/rfc822 part, then body will be a Mailinglist Message
   */
  protected $body;

  /**
   * Build an message part
   * @param $msg The data to build the message from.
   * Expecting a string with the data to build the part
   *
   * @returns Build the MailinglistMessagePart
   */
  function __construct($msg = NULL) {
    if (is_string($msg)) {
      $break = strpos($msg, "\n\n");
      $this->headers = substr($msg, 0, $break+1);
      $this->body = substr($msg, $break+2);

      $this->parseHeaders();
      $this->parseBody();
    }
  }

  /**
   * parseHeaders()
   * Converts the headers member from a single text string with the contents of the
   * part (or message) headers to an associtive array of the headers.
   * The index of the array is the Header name, and the value is the header value.
   * If header was repeated, the value will be an array with each header as an element.
   */
  function parseHeaders() {
    // Unfold headers
    $headers = preg_replace('/\n\s+/', ' ', $this->headers);
    // Convert to an array of headers
    $headers = explode("\n", $headers);
    // Build up the associative array of the headers
    $this->headers = array();
    foreach ($headers as $header) {
      $colon = strpos($header, ': ');
      if ($colon > 0) {
        $header_name = substr($header, 0, $colon);
        $header_value = substr($header, $colon+2);
        // duplicate headers become arrays
        if (isset($this->headers[$header_name])) {
          if (!is_array($this->headers[$header_name])) {
            $this->headers[$header_name] = array($this->headers[$header_name]);
          }
          $this->headers[$header_name][] = $header_value;
        }
        else {
          $this->headers[$header_name] = $header_value;
        }
      }
    }
  }

  /**
   * parseBody())
   * converts text string in the body variable into a processed body
   * For MIME type multipart, the body will become an array of the sections
   * For MIME type message/rfc822, the body will become a MailingMessage object
   *
   * Process encoding, character sets to make data stored as UTF-8
   * Thus the encoding/character set do NOT represent what is in the buffer, but
   * how the data was sent.
   *
   * Notes:
   * Content-Type: Text/plain
   * multipart/mixed  multipart/alternate  multipart/digest
   *   charset= US-ASCII UTF-8 ISO-8859-#
   *
   * Content-Transfer-Encoding: BASE64, Quoted-printable, 7bit, 8bit
   */
  function parseBody() {

    if (isset($this->headers['Content-Transfer-Encoding'])) {
      $header = new MailinglistHeader($this->headers['Content-Transfer-Encoding']);
      $cte = $header->get_atom();

      switch (strtolower($cte)) {
        case 'quoted-printable':
          $this->body = quoted_printable_decode($this->body);
          break;
        case 'base64':
          $this->body = base64_decode($this->body);
          break;
        default:
          $this->logError('Unknown CTE "@cte"', array('@cte' => $cte));
        case 'binary':
        case '8bit':
        case '7bit':
          // These encodings use a null transform, so we can ignore them
          break;
      }
    }
    else {
      $cte = '7bit';
    }

    if (isset($this->headers['Content-Type'])) {
      $header = new MailinglistHeader($this->headers['Content-Type']);
      $ct = $header->get_atom();
      $pos = strpos($ct, '/');
      if (!$pos) {
        $this->logError('Bad Content-Type Format', $ct);
        $pos = 0;
      }
      $type = substr($ct, 0, $pos);
      $subtype = substr($ct, $pos+1);
      $header->get_char();  // Remove the terminator (;) on the atom.
      $parms = array();
      // Get parameters to Content-Type
      while ($parm = $header->get_token()) {
        $char = $header->get_char(); // eat the =
        $value = $header->get_value();
        $parms[$parm] = $value;
      }
      switch (strtolower($type)) {
        case 'multipart':
          $bodies = explode("--" . $parms['boundary'], $this->body);
          $this->body = array();
          foreach ($bodies as $body) {
            if (!empty($body) && $body[0] == "\n" && strlen($body) > 2) {
              $this->body[] = new MailinglistMessagePart(substr($body, 1));
            }
          }
          break;
        case 'message':
          if ($subtype == 'rfc822') {
            $this->body = new MailinglistMessage($this->body);
          }
          break;

        case 'text':
          ///@todo we may want to somehow special mark text/plain sections?
          if (isset($parms['charset'])) {
            $charset = strtoupper($parms['charset']);
            if ($charset == 'US-ASCII') $charset = 'ASCII';
            $encodings = mb_list_encodings();
            $this->body = mb_convert_encoding($this->body, 'UTF-8', $charset);
          }
          break;

        default:
          dpm(array($type, $subtype));
      }
    }
  }

  /**
   * logError()
   *
   * Log a error
   */
  function logError($str, $parm = array()) {
    dpm($str, NULL, 'error');
    watchdog('MailinglistMessage', $str, $parms, WATCHDOG_ERROR);
  }
}

/**
 * Class defining the contents of an e-mail message
 *
 * The differnce between a MailinglistMessage and a MailinglistMessagePart is that
 * a MailinglistMessage represents a "full" RFC822 message, with all the
 * email headers, while a MailinglistMessagePart (when not also a MailinglistMessage)
 * represents just a MIME block with restricted header infomation.
 *
 */
class MailinglistMessage extends MailinglistMessagePart implements MailinglistMessageInterface {
  protected $raw_message;
  /**
   * __construct()
   *
   * Build a MailinglistMessage
   *
   * @param $msg A string containing the contents of the message
   * (includng headers).
   */
  function __construct($msg="") {
    if (is_string($msg)) {
      // Building from raw message

      // Fix line endings, just in case
      $msg = str_replace(array("\n\r", "\r\n", "\r"), "\n", $msg);
      $this->raw_message = $msg;
      parent::__construct($msg);
    }
  }

  /**
   * header()
   *
   * Retrieves a specifed header from the message.
   *
   * @param $name String with the name of the header to get.
   * @returns the value of the header (a string or an array if multiple headers of that type).
   */
  function header($name) {
    if (isset($this->headers[$name])) {
      return $this->headers[$name];
    }
    else return FALSE;
  }
}


