<?php
/**
 * @file
 * Definition of MailinglistPhpImapRetrieve class.
 */

/**
 * Retrieve messages using PHP IMAP library.
 */
class MailinglistPhpImapRetrieve extends MailinglistRetrieve {
  /**
   * @name ctools_export_ui
   *
   * @{
   */
  /**
   * Implements ctools_export_ui::edit_form().
   * called via mailinglist_export_ui
   */
  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);
    global $cookie_domain;

//    $pass_plain = mailinglist_decode_password($pass);

    $form['connection']['settings']['box_type'] = array(
      '#type' => 'select',
      '#title' => t('Protocol'),
      '#options' => array('imap' => 'IMAP', 'pop3' => 'POP3', 'local' => 'Local mbox file'),
      '#default_value' => $this->get_setting('box_type', 'imap'),
      '#description' => t('You can use the IMAP/POP3 protocols, or retrieve from an mbox file on the local file system.'),
      '#required' => TRUE,
  );
    $ajax_settings = array(
      'callback' => '_mailinglist_mailbox_test',
      'wrapper' => 'mailinglist_test_results',
      'event' => 'change',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Please wait - testing connection settings...'),
      ),
    );
    $form['connection']['settings']['folder'] = array(
      '#type' => 'textfield',
      '#title' => t('Folder'),
      '#default_value' => $this->get_setting('folder'),
      '#description' => t('The folder where the mail is stored. If you want this mailbox to read from a local mbox file, give the path relative to the Drupal installation directory.'),
      '#ajax' => $ajax_settings,
      '#required' => TRUE,
  );
    $form['connection']['settings']['domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Domain'),
      '#default_value' => $this->get_setting('domain'),
      '#description' => t('The domain of the server used to collect mail.'),
      '#ajax' => $ajax_settings,
      '#required' => TRUE,
  );
    $form['connection']['settings']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->get_setting('port'),
      '#description' => t('The mailbox port number (usually 110 for POP3, 143 for IMAP).'),
      '#element_validate' => array('element_validate_integer_positive'),
      '#ajax' => $ajax_settings,
      '#required' => TRUE,
  );
    $form['connection']['settings']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->get_setting('name'),
      '#description' => t('This username is used while logging into this mailbox during mail retrieval.'),
      '#ajax' => $ajax_settings,
    );
    $form['connection']['settings']['pass'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => _mailinglist_decode_password($this->get_setting('pass')),
      '#description' => t('The mailbox password corresponding to the username above. Consider using a non-vital password, since this field is stored with minimal encryption in the database and displayed here.'),
      '#ajax' => $ajax_settings,
    );
    // Allow administrators to configure the mailbox with extra IMAP commands (notls, novalidate-cert etc.)
    $form['connection']['settings']['extraimap'] = array(
      '#type' => 'textfield',
      '#title' => t('Extra commands'),
      '#default_value' => $this->get_setting('extraimap'),
      '#description' => t('In some circumstances you need to issue extra commands to connect to your mail server (for example "/notls", "/novalidate-cert" etc.). See documentation for <a href="@imap-open">imap_open</a>.', array('@imap-open' => url('http://php.net/imap_open'))),
      '#ajax' => $ajax_settings,
    );
    $form['connection']['settings']['results'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'mailinglist_test_results',
      ),
    );

    $form['extra']['settings']['flag_after_read'] = array(
      '#type' => 'checkbox',
      '#title' => t('Mark messages as seen/read after they are processed?'),
      '#default_value' => $this->get_setting('flag_after_read', TRUE),
      '#description' => t('Note that messages cannot be marked as seen/read for POP3 accounts.'),
    );
    $form['extra']['settings']['delete_after_read'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete messages after they are processed?'),
      '#default_value' => $this->get_setting('delete_after_read', TRUE),
      '#description' => t('Uncheck this box to leave read messages in the mailbox. They will not be processed again unless they become marked as unread.  If you selected "POP3" as your mailbox type, you must check this box.'),
    );
  }

  /**
   * Implements ctools_export_ui::edit_form_validate().
   * Called via mailinglist_export_ui.
   */
  public function edit_form_validate(&$form, &$form_state) {
    parent::edit_form_validate($form, $form_state);

    // If POP3 mailbox is chosen, messages should be deleted after processing.
    // Do not set an actual error because this is helpful for testing purposes.
    if ($form_state['values']['settings']['box_type'] == 'pop3' && $form_state['values']['settings']['delete_after_read'] == 0) {
      drupal_set_message(t('Unless you check off "Delete messages after they are processed" when using a POP3 mailbox, old emails will be re-imported each time the mailbox is processed. You can partially prevent this by mapping Message ID to a unique target in the processor configuration - see INSTALL.txt or advanced help for more information'), 'warning');
    }
    if (($form_state['values']['settings']['box_type'] == 'pop3' && $form_state['values']['settings']['port'] != 110) || ($form_state['values']['settings']['box_type'] == 'imap' && $form_state['values']['settings']['port'] != 143)) {
      drupal_set_message(t('Non-standard Port:') . ' ' . $form_state['values']['settings']['port'] . ' (' . $form_state['values']['settings']['box_type'] . ')', 'warning');
    }
  }
  /**
   * Implements ctools_export_ui::edit_form_submit().
   * Called via mailinglist_export_ui.
   */
  public function edit_form_submit(&$form, &$form_state) {
    parent::edit_form_submit($form, $form_state);
  }

  /// @}
  /**
   * @name MailinglistRetrieveInterface
   *
   * @{
   */
  /*
   * Connect to mailbox and run message retrieval.
   *
   * @return array
   *   Retrieved messages.
   */
  public function get_message_list($max = 0) {
    dpm($this->settings);
    extract($this->settings);
    $new = array();
    /*
    if ($result = $this->open_mailbox()) {
      $new = $this->get_unread_messages($result);
    }
    else {
      imap_errors();
      throw new Exception(t('Unable to connect to %mail. Please check the <a href="@mailbox-edit">connection settings</a> for this mailbox.', array('%mail' => $this->mail, '@mailbox-edit' => url('admin/structure/mailinglist/list/' . $this->mail . '/edit'))));
    }
    */
    return $new;
  }
  /// @}


/*************************************************************/
  function retrieve() {
    extract($this->settings);
    $result = $this->open_mailbox();
    if ($result) {
      $new = $this->get_unread_messages($result);
    }
    else {
      imap_errors();
      throw new Exception(t('Unable to connect to %mail. Please check the <a href="@mailbox-edit">connection settings</a> for this mailbox.', array('%mail' => $this->mail, '@mailbox-edit' => url('admin/structure/mailinglist/list/' . $this->mail . '/edit'))));
    }
    $messages = array();
    $retrieved = 0;
    while ($new && (!$limit || $retrieved < $limit)) {
      $message = $this->retrieve_message($result, array_shift($new));
      if ($message) {
        $messages[] = $message;
      }
      ++$retrieved;
    }
    watchdog('mailinglist', 'Mailbox %mail was checked and contained %retrieved messages.', array('%mail' => $this->admin_title, '%retrieved' => $retrieved), WATCHDOG_INFO);
    $this->close_mailbox($result);
    return $messages;
  }

  /**
   * Test connection to a mailbox.
   *
   * @return array
   *   Test results.
   */
  function test() {
    extract($this->settings);
    $ret = array();

    $is_local = ($type == 'local');
    $folder_is_set = (!empty($folder) && $folder != 'INBOX');
    $connect_is_set = !empty($domain) && !empty($port) && !empty($name) && !empty($pass);

    if (($is_local && $folder_is_set) || (!$is_local && $connect_is_set)) {
      $result = $this->open_mailbox();
      if ($result) {
        $ret[] = array('severity' => 'status', 'message' => t('Mailinglist was able to connect to the mailbox.'));
        $box = $this->mailbox_string();
        $status = imap_status($result, $box, SA_MESSAGES);
        if ($status) {
          $ret[] = array('severity' => 'status', 'message' => t('There are @messages messages in the mailbox folder.', array('@messages' => $status->messages)));
        }
        else {
          $ret[] = array('severity' => 'warning', 'message' => t('Mailinglist could not open the specified folder'));
        }
        $this->close_mailbox($result);
      }
      else {
        imap_errors();
        $ret[] = array('severity' => 'error', 'message' => t('Mailinglist could not access the mailbox using these settings'));
      }
    }
    return $ret;
  }
  /**
   *
   */
  function purge_message($message) {
    $this->log("Purge Message: " . $message);
    if (isset($message['imap_uid'])) {
      $result = $this->open_mailbox();
      if ($result) {
        if ($this->settings['delete_after_read']) {
          imap_delete($result, $message['imap_uid'], FT_UID);
        }
        elseif (!isset($this->settings['flag_after_read']) || ($this->settings['flag_after_read'])) {
          imap_setflag_full($result, (string)$message['imap_uid'], '\Seen', FT_UID);
        }
        $this->close_mailbox($result);
      }
      else {
        drupal_set_message(t('Unable to connect to mailbox.'));
        watchdog('mailinglist', 'Unable to connect to %mail', array('%mail' => $this->mail), WATCHDOG_ERROR);
      }
    }
  }

  /**
   * Establish IMAP stream connection to specified mailbox.
   *
   * @return
   *   IMAP stream.
   */
  function open_mailbox() {
    extract($this->settings);

    if (!function_exists('imap_open')) {
      throw new Exception(t('The PHP IMAP extension must be enabled in order to use Mailinglist.'));
    }
    $box = $this->mailbox_string();
    if ($type != 'local') {
      $result = imap_open($box, $name, $pass, NULL, 1);
    }
    else {
      $orig_home = getenv('HOME');
      // This is hackish, but better than using $_SERVER['DOCUMENT_ROOT']
      $new_home = realpath(drupal_get_path('module', 'node') . '/../../');
      if (!putenv("HOME=$new_home")) {
        throw new Exception(t('Could not set home directory to %home.', array('%home' => $new_home)));
      }
      $result = imap_open($box, '', '', NULL, 1);
      putenv("HOME=$orig_home");
    }
    return $result;
  }

  /**
   * Constructs a mailbox string based on mailbox object
   */
  function mailbox_string() {
    extract($this->settings);

    switch ($type) {
      case 'imap':
        return '{' . $domain . ':' . $port . $extraimap . '}' . $folder;
      case 'pop3':
        return '{' . $domain . ':' . $port . '/pop3' . $extraimap . '}' . $folder;
      case 'local':
        return $folder;
    }
  }

  /**
   * Returns the first part with the specified mime_type
   *
   * USAGE EXAMPLES - from php manual: imap_fetch_structure() comments
   * $data = get_part($stream, $msg_number, "TEXT/PLAIN"); // get plain text
   * $data = get_part($stream, $msg_number, "TEXT/HTML"); // get HTML text
   */
  function get_part($stream, $msg_number, $mime_type, $structure = FALSE, $part_number = FALSE, $encoding) {
    if (!$structure) {
      $structure = imap_fetchstructure($stream, $msg_number);
    }
    if ($structure) {
      foreach ($structure->parameters as $parameter) {
        if (drupal_strtoupper($parameter->attribute) == 'CHARSET') {
          $encoding = $parameter->value;
        }
      }
      if ($mime_type == $this->get_mime_type($structure)) {
        if (!$part_number) {
          $part_number = '1';
        }
        $text = imap_fetchbody($stream, $msg_number, $part_number, FT_PEEK);
        if ($structure->encoding == ENCBASE64) {
          return drupal_convert_to_utf8(imap_base64($text), $encoding);
        }
        elseif ($structure->encoding == ENCQUOTEDPRINTABLE) {
          return drupal_convert_to_utf8(quoted_printable_decode($text), $encoding);
        }
        else {
          return drupal_convert_to_utf8($text, $encoding);
        }
      }
      if ($structure->type == TYPEMULTIPART) { /* multipart */
        $prefix = '';
        while (list($index, $sub_structure) = each($structure->parts)) {
          if ($part_number) {
            $prefix = $part_number . '.';
          }
          $data = $this->get_part($stream, $msg_number, $mime_type, $sub_structure, $prefix . ($index + 1), $encoding);
          if ($data) {
            return $data;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Returns an array of parts as file objects
   *
   * @param $stream
   *  Handle to stream to fetch the message from
   * @param $msg_number
   *  message number to retreive.
   * @param $max_depth
   *   Maximum Depth to recurse into parts.
   * @param $depth
   *   The current recursion depth.
   * @param $structure
   *   A message structure, usually used to recurse into specific parts
   * @param $part_number
   *   A message part number to track position in a message during recursion.
   * @return
   *   An array of file objects.
   */
  function get_parts($stream, $msg_number, $max_depth = 10, $depth = 0, $structure = FALSE, $part_number = 1) {
    $parts = array();

    // Load Structure.
    if (!$structure) {
      $structure = imap_fetchstructure($stream, $msg_number);
      if (!$structure)
        watchdog('mailinglist', 'Could not fetch structure for message number %msg_number', array('%msg_number' => $msg_number), WATCHDOG_NOTICE);
        return $parts;
    }
    // Recurse into multipart messages.
    if ($structure->type == TYPEMULTIPART) {
      // Restrict recursion depth.
      if ($depth >= $max_depth) {
        watchdog('mailinglist', 'Maximum recursion depths met in mailhander_get_structure_part for message number %msg_number.',   array('%msg_number' => $msg_number), WATCHDOG_NOTICE);
        return $parts;
      }
      $prefix = '';
      foreach ($structure->parts as $index => $sub_structure) {
        // If a part number was passed in and we are a multitype message, prefix the
        // the part number for the recursive call to match the imap4 dot seperated part indexing.
        if ($part_number) {
          $prefix = $part_number . '.';
        }
        $sub_parts = $this->get_parts($stream, $msg_number, $max_depth, $depth + 1,
          $sub_structure, $prefix . ($index + 1));
        $parts = array_merge($parts, $sub_parts);
      }
      return $parts;
    }

    // Per Part Parsing.

    // Initalize file object like part structure.
    $part = new stdClass();
    $part->attributes = array();
    $part->filename = 'unnamed_attachment';
    if (!$part->filemime = $this->get_mime_type($structure)) {
      watchdog('mailinglist', 'Could not fetch mime type for message part. Defaulting to application/octet-stream.', array(), WATCHDOG_NOTICE);
      $part->filemime = 'application/octet-stream';
    }

    if ($structure->ifparameters) {
      foreach ($structure->parameters as $parameter) {
        switch (drupal_strtoupper($parameter->attribute)) {
          case 'NAME':
          case 'FILENAME':
            $part->filename = $parameter->value;
            break;
          default:
            // put every thing else in the attributes array;
            $part->attributes[$parameter->attribute] = $parameter->value;
        }
      }
    }

    // Handle Content-Disposition parameters for non-text types.
    if ($structure->type != TYPETEXT && $structure->ifdparameters) {
      foreach ($structure->dparameters as $parameter) {
        switch (drupal_strtoupper($parameter->attribute)) {
          case 'NAME':
          case 'FILENAME':
            $part->filename = $parameter->value;
            break;
          // put every thing else in the attributes array;
          default:
            $part->attributes[$parameter->attribute] = $parameter->value;
        }
      }
    }

    // Store part id for reference.
    if (!empty($structure->id)) {
      $part->id = $structure->id;
    }

    // Retrieve part and convert MIME encoding to UTF-8
    if (!$part->data = imap_fetchbody($stream, $msg_number, $part_number, FT_PEEK)) {
      watchdog('mailinglist', 'No Data!!', array(), WATCHDOG_ERROR);
      return $parts;
    }

    // Decode as necessary.
    if ($structure->encoding == ENCBASE64) {
      $part->data = imap_base64($part->data);
    }
    elseif ($structure->encoding == ENCQUOTEDPRINTABLE) {
      $part->data = quoted_printable_decode($part->data);
    }
    // Convert text attachment to UTF-8.
    elseif ($structure->type == TYPETEXT) {
      $part->data = imap_utf8($part->data);
    }

    // Always return an array to satisfy array_merge in recursion catch, and
    // array return value.
    $parts[] = $part;
    return $parts;
  }

  /**
   * Retrieve MIME type of the message structure.
   */
  function get_mime_type(&$structure) {
    static $primary_mime_type = array('text', 'multipart', 'message', 'application', 'audio', 'image', 'video', 'other');
    $type_id = (int) $structure->type;
    if (isset($primary_mime_type[$type_id]) && !empty($structure->subtype)) {
      return $primary_mime_type[$type_id] . '/' . drupal_strtolower($structure->subtype);
    }
    return 'text/plain';
  }

  /**
   * Obtain the number of unread messages for an imap stream
   *
   * @param $result
   *   IMAP stream - as opened by imap_open
   * @return array
   *   IMAP message numbers of unread messages.
   */
  function get_unread_messages($result) {
    $unread_messages = array();
    $number_of_messages = imap_num_msg($result);
    for ($i = 1; $i <= $number_of_messages; $i++) {
      $header = imap_header($result, $i);
      if ($header->Unseen == 'U' || $header->Recent == 'N') {
        $unread_messages[] = $i;
      }
    }
    return $unread_messages;
  }

  /**
   * Retrieve individual messages from an IMAP result.
   *
   * @param $result
   *   IMAP stream.
   * @param $msg_number
   *   IMAP message number.
   * @return array
   *   Retrieved message, or FALSE if message cannot / should not be retrieved.
   */
  function retrieve_message($result, $msg_number) {
    extract($this->settings);
    dpm("MailinglistPhpImapRetreive::retreive_message");
    $raw_headers = imap_fetchheader($result, $msg_number, FT_PREFETCHTEXT);
    ///@todo parse raw_headers
    $header = imap_headerinfo($result, $msg_number);

    // Initialize the subject in case it's missing.
    if (!isset($header->subject)) {
      $header->subject = '';
    }
    // Parse MIME parts, so all mailinglist modules have access to
    // the full array of mime parts without having to process the email.
    $mimeparts = $this->get_parts($result, $msg_number);

    $body_text = $this->get_part($result, $msg_number, 'text/plain', FALSE, FALSE, $encoding);
    $body_html = $this->get_part($result, $msg_number, 'text/html', FALSE, FALSE, $encoding);
    if (!$body_text && $body_html) {
      $body_text = $body_html;
    }
    elseif ($body_text && !$body_html) {
      $body_html = $body_text;
    }
    // Is this an empty message with no body and no mimeparts?
    if (!$body_text && !$body_html && !$mimeparts) {
      $message = FALSE;
    }
    else {
      $imap_uid = ($type == 'pop3') ? $this->fetch_uid($msg_number) : imap_uid($result, $msg_number);
      $message = compact('header', 'raw_headers', 'body_text', 'body_html', 'mimeparts', 'imap_uid');
    }
    dpm($message);
    return $message;
  }

  /**
   * Close a mailbox.
   */
  function close_mailbox($result) {
    imap_errors();
    imap_close($result, CL_EXPUNGE);
  }

  /**
   * Fetch UID for a message in a POP mailbox.
   *
   * Taken from PHP.net.
   */
  function fetch_uid($msg_number) {
    extract($this->settings);
    $retval = 0;
    $fp = fsockopen($domain, $port);
    if ($fp > 0) {
      $buf = fgets($fp, 1024);
      fwrite($fp, "USER $name\r\n");
      $buf = fgets($fp, 1024);
      fwrite($fp, "PASS $pass\r\n");
      $buf = fgets($fp, 1024);
      fwrite($fp, "UIDL $msg_number\r\n");
      $retval=fgets($fp, 1024);
      fwrite($fp, "QUIT\r\n");
      $buf = fgets($fp, 1024);
      fclose($fp);
    }
    return drupal_substr($retval, 6, 30);
  }
}
