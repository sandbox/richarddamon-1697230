<?php
/**
 * @file
 * MailinglistPhpImapRetrieve class.
 */

$plugin = array(
  'name' => 'Mailinglist PHP IMAP',
  'description' => 'Extends Mailhandlers Imap handler to get full headers of message',
  'handler' => array(
    'class' => 'MailinglistPhpImapRetrieve',
    'parent' => 'MailinglistRetrieve'
  ),
  'file' => 'MailinglistPhpImapRetrieve.class.php',
);
