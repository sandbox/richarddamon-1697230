<?php
/**
 * @file
 * MailinglistRetrieve class.
 */

$plugin = array(
  'hidden' => TRUE,
  'handler' => array(
    'class' => 'MailinglistRetrieve',
    'parent' => 'MailinglistExportable',
  ),
  'file' => 'MailinglistRetrieve.class.php',
);

