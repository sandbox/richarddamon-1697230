<?php
/**
 * @file
 * Definition of mailinglist_mailbox_ui class.
 */

/**
 * Represents an email mailbox (IMAP, POP3, etc...).
 *
 */
class MailinglistRetrieveUI extends MailinglistExportUI {
  /**
   * @name ctools_export_ui
   * @{
   */

  /**
   * Implements ctools_export_ui::hook_form().
   */
  function hook_menu(&$items) {
    parent::hook_menu($items);
    if (isset($items['admin/config/system/mailinglist/mailbox'])) {
        $items['admin/config/system/mailinglist/mailbox']['type'] = MENU_LOCAL_TASK;
    }
  }

  /**
   * Implements ctools_export_ui::edit_form().
   */
  function edit_form(&$form, &$form_state) {

    // All Retrievers will need some settings for connection.
    $form['connection']['#tree'] = FALSE;
    $form['connection']['#weight'] = 20;
    $form['connection']['settings']= array(
      '#type' => 'fieldset',
      '#title' => 'Mailbox connection settings',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['extra']['#tree'] = FALSE;
    $form['extra']['#weight'] = 90;
    $form['extra']['settings']= array(
      '#type' => 'fieldset',
      '#title' => 'More settings',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    parent::edit_form($form, $form_state);

    // object_type selection is not dependant on type of object

    $retrieve_plugins = mailinglist_get_plugins('mailinglist', 'retrieve');
    /* Type of Mail Box */
    $form['info']['object_type'] = array(
      '#type' => 'select',
      '#title' => t('Mailbox Type'),
      '#options' => _mailinglist_build_options($retrieve_plugins),
      '#default_value' => isset($form_state['item']->object_type) ? $form_state['item']->object_type : NULL,
      '#description' => t('Plug in to use to read mailbox'),
      '#required' => TRUE,
    );
  }

  /**
   * list_header()
   */
  function list_header($form_state) {
    if (isset($form_state['input']['test_result'])) {
      return $form_state['input']['test_result'];
    }
  }

  /**
   * Callback to test a mailbox connection.
   */
  function test_page($js, $input, $mailbox) {
    $input['test_result'] = _mailinglist_mailbox_test_output($mailbox);
    if (!$js) {
      drupal_goto(ctools_export_ui_plugin_base_path($this->plugin));
    }
    else {
      return $this->list_page($js, $input);
    }
  }

  /**
   * process_page()
   *
   * UI Callback for mailinglist_mailbox_ui process operation
   */
  function process_page($js, $input, $mailbox) {
//  dpm($js);
//  dpm($input);
    dpm($mailbox, 'process_page');
    $page['list'] = array(
      '#theme' => 'table',
      '#header' => array(
        array('data' => 'Message', ),
        array('data' => 'List'),
        array('data' => 'Results', ),
        array('data' => 'Subject', ),
      ),
      '#rows' => array(),
    );

    drupal_set_title($this->get_page_title('process', $mailbox));

    // Check if we have a real mailbox object
    if (method_exists($mailbox, 'get_message_list')) {
      $lists = mailinglist_list_load_all();
      $msg_list = $mailbox->get_message_list($mailbox->settings['limit']);
      if (!$msg_list) {
        // Avoid error message on error, maybe should log the error?
        $msg_list = array();
      }
      foreach ($msg_list as $msg_idx => $msg_id) {
        $msg = $mailbox->get_message($msg_id);
        dpm($msg, $msg_id);
        $handled = FALSE;
        foreach ($lists as $list => $listobj) {
          $handled = $listobj->processMessage($msg);
          if ($handled) {
            if ($mailbox->get_setting('delete_after_read', FALSE)) {
                dpm('Purge', $msg_id);
//              $mailbox -> purge_msg($msg_id);
            }
            break;
          }
        } // end of foreach ($lists), process handled code
        if ($handled == FALSE) {
          $list = "**None**";
          $handled = "Unhandled";
        }
        if ($handled === TRUE) {
          $handled = "Match";
        }
        $row = array($msg_id, $list, $handled, $msg->header('Subject'));
        $msg_list[$msg_idx] = array($msg_id, $handled ? $list : 'Unhandled');
        $page['list']['#rows'][$msg_idx] = $row;
      }
      $mailbox->close();
    }
    return $page;
  }
  /// {@
}
