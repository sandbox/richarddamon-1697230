<?php
/**
 * @file
 * Allows Mailinglist Mailboxes to be exported.
 */

$plugin = array(
  'schema' => 'mailinglist_retrieve',
  'access' => 'administer mailinglist',
  'menu' => array(
    'menu prefix' => 'admin/config/system/mailinglist',
    'menu item' => 'mailbox',
    'menu title' => 'Mailboxes',
    'menu description' => 'Administer mailinglist mailboxes.',
    'items' => array(
      'test' => array(
        'path' => 'list/%ctools_export_ui/test',
        'title' => 'Test connection',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => array('MailinglistRetrieveUI', 'test', 4),
        'load arguments' => array('MailinglistRetrieveUI'),
        'file' => 'includes/export-ui.inc',
        'file path' => drupal_get_path('module', 'ctools'),
        'access arguments' => array('administer mailinglist'),
        'type' => MENU_CALLBACK,
      ),
      'process' => array(
        'path' => 'list/%ctools_export_ui/process',
        'title' => 'Process Mailbox',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => array('MailinglistRetrieveUI', 'process', 6),
        'load arguments' => array('MailinglistRetrieveUI'),
        'file' => 'includes/export-ui.inc',
        'file path' => drupal_get_path('module', 'ctools'),
        'access arguments' => array('administer mailinglist'),
        'type' => MENU_CALLBACK,
      ),
    ),
  ),
  'strings' => array(
    'title' => array(
      'process' => 'Process %title',
      'list'    => 'List of Mailboxes'
    ),
  ),
  'title singular' => t('mailinglist mailbox'),
  'title singular proper' => t('Mailinglist Mailbox'),
  'title plural' => t('mailinglist mailboxes'),
  'title plural proper' => t('Mailinglist Mailboxes'),
  'handler' => array(
    'class' => 'MailinglistRetrieveUI',
    'parent' => 'mailinglist_export_ui',
  ),
  'allowed operations' => array(
    'test' => array('title' => t('Test connection'),
      'ajax' => TRUE,
      'token' => TRUE),
    'process' => array('title' => t('Process Mailbox')),
  ),
  'file' => 'MailinglistRetrieveUI.class.php',
);
