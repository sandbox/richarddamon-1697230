<?php
/**
 * @file
 * Definition of mailinglist_export_ui class.
 */

/**
 * Place for common code for export_ui classes
 * Forwards a lot of ctools_export_ui hooks to the exportable class
 */
abstract class MailinglistExportUI extends ctools_export_ui {
  /**
   * @name ctools_export_ui
   * @{
   */

  /**
   * Implements ctools_export_ui::edit_form().
   *
   * @todo Need to decide how to handle this:
   * When building the form, we have in $form_state['item'] the value of the
   * object for this form as read from the database to build up the form.
   * The issue is that this does NOT reflect changes made in the form along the way
   * until we get through Submit.
   * The big issue is that if we change what type of object we want, we NEED to
   * change the object to that time, so as to build the form for the needed values
   * but the object in the form_state will not really be 'valid' as fields needed
   * by the type (and provided by the form) may not be present.
   * We also have a quandry on taking other values from the input, if we take them
   * at form building time, they haven't (and can't) be validated (can't because
   * we don't have form info to do the validation). Is it better to be missing some
   * values, and others be stale, or to have more (but likely not all) values but
   * from an unvalidated source.
   */
  public function edit_form(&$form, &$form_state) {
    dpm($form_state, 'mailinglist_export_ui::edit_form');
    $form_state['orig_item'] = clone($form_state['item']);
    // Check if we need to rebuild object to change its type.

    if (isset($form_state['input']['object_type']) && $form_state['input']['object_type'] != $form_state['item']->object_type) {
      // Request Type different than current type lets change it to get right menu
      $export_type = $form_state['item']->export_type;
      $schema = ctools_export_get_schema($form_state['plugin']['schema']);
      $data = (array) $form_state['item'];
      /// @todo Should we do this? (see above), maybe just copy 'object_type'
      $data = mailinglist_array_merge_recursive_distinct($data, $form_state['input']);
      dpm($schema);
      dpm($form_state);
      dpm($data);
      // Serialize the fields that need it
      foreach ($schema['fields'] as $field => $field_schema) {
        if (isset($field_schema['serialize']) && $field_schema['serialize']) {
          $data[$field] = serialize($data[$field]);
        }
      }
      $data = (object)$data;
      $form_state['item'] = _mailinglist_object_factory($schema, $data);
      $form_state['item']->export_type = $export_type; ///@todo why doesn't this get copied?
    }
    parent::edit_form($form, $form_state);

    // disable caching as it gets in the way of changing the object's type.
    ///@todo see if we can normally cache, and only disable cache if changing object type.
    $form_state['no_cache'] = TRUE;

    if (method_exists($form_state['item'], 'edit_form')) {
      $form_state['item']->edit_form($form, $form_state);
    }

    $form['info']['object_type_menu'] = array(
      '#type' => 'hidden',
      '#value' => isset($form_state['item']->object_type) ? $form_state['item']->object_type : NULL,
    );
  }

  /**
   * Implements ctools_export_ui::edit_form_validate().
   */
  function edit_form_validate(&$form, &$form_state) {
    dpm($form_state, 'mailinglist_export_ui::edit_form_validate');
    if ($form_state['input']['object_type_menu'] != $form_state['input']['object_type']) {
      ///@todo Why doesn't this flag the error?
      form_set_error('object_type', 'Type Changed, Check Settings!');
      watchdog('Mailinglist', 'Type Change Verify', array(), WATCHDOG_DEBUG);
    }

    parent::edit_form_validate($form, $form_state);

    if (method_exists($form_state['item'], 'edit_form_validate')) {
      $form_state['item']->edit_form_validate($form, $form_state);
    }
  }

  /**
   * Implements ctools_export_ui::edit_form_submit().
   */
  function edit_form_submit(&$form, &$form_state) {
    dpm($form_state, 'mailinglist_export_ui::edit_form_submit');
    // Do our hook before parent:: as parent copies over item, and there may be values there we want save in settings.
    if (method_exists($form_state['item'], 'edit_form_submit')) {
      $form_state['item']->edit_form_submit($form, $form_state);
    }

    parent::edit_form_submit($form, $form_state);
  }

  /*
   * Implements ctools_export_ui::edit_save_form()
   *
   * Called on saving object (which is the main part of ctools_export)ui::edit_save_form()
   * generate hooks on items being created or changed.
   */
  function edit_save_form($form_state) {
    dpm(debug_backtrace(), 'mailinglist_export_ui::edit_save_form()');

    // This call will end up doing the actual form save
    parent::edit_save_form($form_state);
    $item = &$form_state['item'];
    if ($form_state['op'] == 'add') {
      $item->table = $this->plugin['schema']; // New objects don't seem to get a table item.
      $item->created();
    }
    else {
      $orig_item = $form_state['orig_item'];
      if ($item->object_type != $orig_item->object_type) {
        $item->changedType($orig_item);
      }

      $item->edited($orig_item);
    }
  }

  /**
   * Implements ctools_export_ui::delete_form_submit().
   *
   * Hook into delete_form_submit to generate a hook for item ready to disappear.
   */
  function delete_form_submit(&$form_state) {
    $item = $form_state['item'];
    dpm($item, 'Delete');
    $item->delete();
    parent::delete_form_submit($form_state);
  }
/// @}
  /* Add any other hooks that might be needed here, like above */

};


