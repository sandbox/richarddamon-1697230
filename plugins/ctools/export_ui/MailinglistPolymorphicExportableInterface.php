<?php
/**
 * @file
 * Definition of MailinglistPolymorphicExportableInterface interface.
 */

/**
 * interface MailinglistPolymorphicExportInterface
 */
interface MailinglistPolymorphicExportableInterface {
  /**
   * @name MailinglistPolymorphicExportableInterface
   * @{
   */

  /**
   * created()
   *
   * hook called when a new object has been created in the database.
   */
  function created();

  /**
   * edited()
   *
   * hook called when an object has been updated in the database.
   *
   * @param $old previous value of the object
   */
  function edited(MailinglistPolymorpicExportableInterface $old);

  /**
   * changedType()
   *
   * hook called when an object has had its object_type changed in the database.
   *
   * @param $old previous value of the object
   */
  function changedType(MailinglistPolymorpicExportableInterface $old);

  /**
   * delete()
   *
   * hook called when an object is about to be deleted from the database.
   *
   */
  function delete();

/// @}
};

