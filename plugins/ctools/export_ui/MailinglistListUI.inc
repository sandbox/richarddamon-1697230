<?php
/**
 * @file
 * Allows Mailinglist Mailboxes to be exported.
 */

$plugin = array(
  'schema' => 'mailinglist_list',
  'access' => 'administer mailinglist',
  'menu' => array(
    'menu prefix' => 'admin/config/system/mailinglist',
    'menu item' => 'list',
    'menu title' => 'Mailing Lists',
    'menu description' => 'Administer mailinglist lists.',
    'items' => array(
      'test' => array(
        'path' => 'list/%ctools_export_ui/test',
        'title' => 'Test connection',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => array('mailinglist', 'test', 4),
        'load arguments' => array('mailinglist'),
        'file' => 'includes/export-ui.inc',
        'file path' => drupal_get_path('module', 'ctools'),
        'access arguments' => array('administer mailinglist'),
        'type' => MENU_CALLBACK,
      ),
    )
  ),
  'title singular' => t('mailinglist list'),
  'title singular proper' => t('Mailinglist List'),
  'title plural' => t('mailinglist list'),
  'title plural proper' => t('Mailinglist Lists'),
  'handler' => array(
    'class' => 'MailinglistListUI',
    'parent' => 'mailinglist_export_ui',
  ),
  'allowed operations' => array(
    'test' => array('title' => t('Test connection'),
    'ajax' => TRUE,
    'token' => TRUE),
  ),
  'file' => 'MailinglistListUI.class.php',
);
