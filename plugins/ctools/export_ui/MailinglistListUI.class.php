<?php
/**
 * @file
 * Definition of mailinglist_list_ui class.
 */

/**
 * Represents an Mailinglist personality module.
 *
 */
class MailinglistListUI extends MailinglistExportUI {
  /**
   * @name ctools_export_ui
   * @{
   */

  /**
   * Implements ctools_export_ui::edit_form().
   */
  function edit_form(&$form, &$form_state) {
    // All Retrievers will need some settings for connection.
    $form['basic']['#tree'] = FALSE;
    $form['basic']['#weight'] = 20;
    $form['basic']['settings']= array(
      '#type' => 'fieldset',
      '#title' => 'Basic List settings',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['extra']['#tree'] = FALSE;
    $form['extra']['#weight'] = 90;
    $form['extra']['settings']= array(
      '#type' => 'fieldset',
      '#title' => 'Advanced Settings',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );


    parent::edit_form($form, $form_state);
    // object_type selection is not dependant on type of object

    $list_plugins = mailinglist_get_plugins('mailinglist', 'list');
    /* Type of Mail Box */
    $form['info']['object_type'] = array(
      '#type' => 'select',
      '#title' => t('List Type'),
      '#options' => _mailinglist_build_options($list_plugins),
      '#default_value' => isset($form_state['item']->object_type) ? $form_state['item']->object_type : NULL,
      '#description' => t('The List presonality plugin to use for this list'),
      '#required' => TRUE,
    );
  }

  /**
   * Implements ctools_export_ui:hook_menu()
   */
  function hook_menu(&$items) {
    parent::hook_menu($items);
    if (isset($items['admin/config/system/mailinglist/list'])) {
        $items['admin/config/system/mailinglist/list']['type'] = MENU_LOCAL_TASK;
    }
  }

  /**
   * list_header()
   */
  function list_header($form_state) {
    if (isset($form_state['input']['test_result'])) {
      return $form_state['input']['test_result'];
    }
  }

  /**
   * Callback to test a mailbox connection.
   */
  function test_page($js, $input, $mailbox) {
    $input['test_result'] = _mailinglist_mailbox_test_output($mailbox);
    if (!$js) {
      drupal_goto(ctools_export_ui_plugin_base_path($this->plugin));
    }
    else {
      return $this->list_page($js, $input);
    }
  }
  /// @}
}

