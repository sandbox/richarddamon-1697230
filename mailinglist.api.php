<?php

/**
 * @file
 * API definition file for the mailinglist module
 *
 */


 /**
  *
  */
function hook_mailinglist_message($message, $list) {}

function hook_mailinglist_message_alter($message, $list) {}

function hook_mailinglist_list_created($list);

function hook_mailinglist_list_edited($list);

function hook_mailinglist_list_changed_type($list);

function hook_mailinglist_list_delete($list);

function hook_mailinglist_retrieve_created($list);

function hook_mailinglist_retrieve_edited($list);

function hook_mailinglist_retrieve_changed_type($list);

function hook_mailinglist_retrieve_deleted($list);
