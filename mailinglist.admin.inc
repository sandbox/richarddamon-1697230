<?php
/**
 * @file
 * Mailinglist admin menus.
 *
 */

/**
 * _mailinglist_config_form()
 *
 * Implements hook_form().
 * Menu for admin/config/system/mailinglist
 * @see _mailinglist_config_form_validate()
 * @see _mailinglist_config_form_submit()
 */
function _mailinglist_config_form($form, &$form_state) {
  dpm($form);
  dpm($form_state);
  $form = array_merge($form, array(
    'submit' => array(
      '#type' => 'submit',
      // @ignore style_button_submit
      '#value' => t('Submit'),
    ),
    'mailinglist_base' => array(
        '#weight' => 10,
        '#type' => 'textfield',
        '#title' => t('Mailinglist Base Page'),
        '#description' => t('Relative URL for user access to mailing list pages'),
        '#required' => TRUE,
        '#default_value' => variable_get('mailinglist_base', 'mailinglist'),
      ),
  ));
  return $form;
}

/**
 * _mailinglist_config_form_validate()
 *
 * Implements hook_form_validate().
 */
function _mailinglist_config_form_validate($form, &$form_state) {
  dpm($form);
  dpm($form_state);

  ///@todo validate mailinglist_base
}

/**
 * _mailinglist_config_form_submit()
 *
 * Implements hook_form_submit().
 */
function _mailinglist_config_form_submit($form, &$form_state) {
  dpm($form);
  dpm($form_state);
  $need_menu_rebuild = FALSE;

  $mailinglist_base = variable_get('mailinglist_base');
  $new_mailinglist_base = $form_state['values']['mailinglist_base'];
  if ($mailinglist_base != $new_mailinglist_base) {
    $need_menu_rebuild = TRUE;
    variable_set('mailinglist_base', $new_mailinglist_base);
    dpm($new_mailinglist_base);
  }

  if ($need_menu_rebuild) {
    menu_rebuild();
  }
}
